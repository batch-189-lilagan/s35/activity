const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const port = 3001;

const app = express();
dotenv.config();

// Connect to our database
mongoose.connect(`mongodb+srv://charleslilagandb:${process.env.MONGODB_PASS}@charleslilagan.chlqocw.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));
db.on('open', () => console.log('Connected to MongoDB!'));


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// 


// Create a User Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

// Create a User Model
const User = mongoose.model('User', userSchema);


// 


// [Creating Routes]
app.post('/signup', (request, response) => {
    User.findOne({name: request.body.username}, (error, result) => {

        // If we encounter error upon .findOne
        if (error) {
            return response.send(error);
        };

        // If we have a result do this...
        if (result != null && result.name == request.body.username) {
            return response.send(`Username ${request.body.username} already exist in our database!`);
        }

        else {
            
            let newUser = new User({
                username: request.body.username,
                password: request.body.password
            });

            newUser.save((error, result) => {
                // If we encounter error upon .save
                if (error) {
                    return console.error(error);
                }

                // If no error proceed...
                return response.status(201).send(`User ${request.body.username} has been registered`);
            });
        }

    });
});

app.listen(port, () => console.log(`Server is running at port ${port}`))